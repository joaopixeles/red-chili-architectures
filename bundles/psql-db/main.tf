
module "SubnetDB" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/subnet?ref=0.0.01-beta"

  subnets               = var.subnets
  prefix                = "db"
  vpc_id                = var.vpc_id
  domain_name           = var.domain_name
  auto_assing_public_ip = true
  tags                  = var.tags
}
module "SecurityGroupPG" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/database/security-group?ref=0.0.01-beta"

  domain_name = var.domain_name
  vpc_id      = var.vpc_id
  tags        = var.tags
}
module "DBPGParameterGroup" {
  source      = "git@gitlab.com:joaopixeles/architectures.git//modules/database/pg-parameter-group?ref=0.0.01-beta"
  domain_name = var.domain_name
  tags        = var.tags
}
module "DBSubnetGroup" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/database/subnet-group?ref=0.0.01-beta"

  domain_name     = var.domain_name
  tags            = var.tags
  subnet_ids_list = module.SubnetDB.subnet_ids
}
module "IAMRoleDBMonitoring" {
  source      = "git@gitlab.com:joaopixeles/architectures.git//modules/database/iam-role-monitoring?ref=0.0.01-beta"
  domain_name = var.domain_name
}
module "Postgresql" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/database/postgresql?ref=0.0.01-beta"

  allocated_storage   = var.allocated_storage
  engine_version      = var.engine_version
  instance_type       = var.instance_type
  storage_type        = var.storage_type
  iops                = var.iops
  database_identifier = var.database_identifier
  snapshot_identifier = var.snapshot_identifier
  database_name       = var.database_name
  database_username   = var.database_username
  database_password   = var.database_password
  database_port       = var.database_port
  publicly_accessible = var.publicly_accessible

  backup_retention_period    = var.backup_retention_period
  backup_window              = var.backup_window
  maintenance_window         = var.maintenance_window
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  final_snapshot_identifier  = var.final_snapshot_identifier
  skip_final_snapshot        = var.skip_final_snapshot

  copy_tags_to_snapshot   = var.copy_tags_to_snapshot
  multi_availability_zone = var.multi_availability_zone
  storage_encrypted       = var.storage_encrypted
  monitoring_interval     = var.monitoring_interval
  deletion_protection     = var.deletion_protection
  cloudwatch_logs_exports = var.cloudwatch_logs_exports
  tags                    = var.tags

  security_group_id       = module.SecurityGroupPG.security_group.id
  subnet_group_name       = module.DBSubnetGroup.db_subnet_group.name
  parameter_group_name    = module.DBPGParameterGroup.db_parameter_group.name
  iam_role_monitoring_arn = module.IAMRoleDBMonitoring.iam_role.arn
}
module "DBMonitoringCloudWatch" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/database/monitoring-cloudwatch?ref=0.0.01-beta"

  domain_name    = var.domain_name
  db_instance_id = module.Postgresql.db_instance.id
}

