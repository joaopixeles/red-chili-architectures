variable "domain_name" {
  type = string
}
variable "tags" {
  default     = {}
  type        = map(string)
  description = "Extra tags to attach to the RDS resources"
}
variable "vpc_id" {
  type = string
}
variable "subnets" {
  type = map(string)
}
