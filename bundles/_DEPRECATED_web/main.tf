module "BucketRoot" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/bucket?ref=0.0.01-beta"

  domain_name = var.domain_name
  WWWBucket   = false
  tags        = var.tags
}

module "CloudFrontCDN" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/cloudfront?ref=0.0.01-beta"

  domain_name         = var.domain_name
  s3_id               = module.BucketRoot.s3_bucket.id
  s3_website_endpoint = module.BucketRoot.s3_bucket.website_endpoint
  certificate_arn     = data.aws_acm_certificate.website.arn
  tags                = var.tags
}

module "Route53Website" {
  source = "git@gitlab.com:joaopixeles/architectures.git//modules/web-route53?ref=0.0.01-beta"

  domain_name            = var.domain_name
  domain_name_principal  = var.domain_name_principal
  cloudfront_domain_name = module.CloudFrontCDN.cloudfront_distribution.domain_name
}