module "BundlesWebviewsTest" {
  source = "../"

  providers = {
    aws.virginia = aws.virginia
  }

  domain_name           = "test.joaopixeles.io"
  domain_name_principal = "joaopixeles.io"
  //
  # execution_role_policy_name = "auth-lambdaatedge"
  # execution_role_name        = "auth-lambdaatedge"
  function_name              = "test-joaopixeles-io"
  handler                    = "s_authorizer.handler"
  runtime                    = "nodejs12.x"
  bucket_name                = "joaopixeles-io-artifacts"
  bucket_filepath            = "serverles/test/auth-cf-custom/pro-a/1614106367948-2021-02-23T18:52:47.948Z/authorizer.zip"
  tags = {
    "Environment" = "DEBUG",
    "Application" = "cloudfront.test.joaopixeles.io"
    "WhoDeployed" = "joao@serendipia.co"
  }

}
