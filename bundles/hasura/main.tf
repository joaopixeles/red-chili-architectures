provider "aws" {
  region = "us-east-1"
  alias  = "certificates"
}

provider "aws" {
  region = "us-west-2"
  alias  = "dns"
}

module "Certificate" {
  source = "../../modules/cross/acm-certificate"

  providers = {
    aws.acm_account     = "aws.certificates"
    aws.route53_account = "aws.dns"
  }

  domain_name              = var.domain_name
  alternative_domain_names = ["*.${var.domain_name}"]
  domain_name_principal    = var.domain_name_principal
  ttl                      = "60"
  allow_overwrite          = true
  tags                     = var.tags
}
module "SecurityGroupLoadbalancer" {
  source = "../../modules/hasura/security-group-lb"

  domain_name = var.domain_name
  vpc_id      = var.vpc_id
  tags        = var.tags
}
module "SecurityGroupRuleDBAccess" {
  source = "../../modules/hasura/security-group-rule-db-access"

  db_port              = var.db_port
  description          = "Allow Hasura to Access DB"
  db_security_group_id = data.aws_security_group.postgresql.id
  lb_security_group_id = module.SecurityGroupLoadbalancer.security_group.id
}
module "LB" {
  source = "../../modules/hasura/lb"

  domain_name          = var.domain_name
  lb_security_group_id = module.SecurityGroupLoadbalancer.security_group.id
  lb_subnet_public_ids = data.aws_subnet_ids.postgresql.ids
  tags                 = var.tags
}
module "LBTargetGroup" {
  source = "../../modules/hasura/lb-target-group"

  module_depends_on        = ["${module.LB.lb}"]
  vpc_id                   = var.vpc_id
  lb_name                  = module.LB.lb.name
  hasura_health_check_path = var.hasura_health_check_path
  tags                     = var.tags
}
module "LBListenerHTTP" {
  source = "../../modules/hasura/lb-listener-http"

  lb_arn = module.LB.lb.arn
  module_depends_on = [
    "${module.LBTargetGroup.lb_target_group}",
    "${module.LB.lb}"
  ]
}
module "LBListenerHTTPS" {
  source = "../../modules/hasura/lb-listener-https"

  certificate_arn     = module.Certificate.acm_certificate.arn
  lb_arn              = module.LB.lb.arn
  lb_target_group_arn = module.LBTargetGroup.lb_target_group.arn
  module_depends_on = [
    "${module.LBTargetGroup.lb_target_group}",
    "${module.LB.lb}"
  ]
}
module "LBListenerRule" {
  source = "../../modules/hasura/lb-listener-rule"

  lb_listener_arn     = module.LBListenerHTTP.lb_listener.arn
  lb_target_group_arn = module.LBTargetGroup.lb_target_group.arn
}
module "IAMRole" {
  source = "../../modules/hasura/iam-role"

  domain_name = var.domain_name
  tags        = var.tags
}
module "Route53LoadBalancer" {
  source = "../../modules/hasura/route53-loadbalancer"

  domain_name           = var.domain_name
  domain_name_principal = var.domain_name_principal
  lb_dns_name           = module.LB.lb.dns_name
  lb_zone_id            = module.LB.lb.zone_id
}
module "IAMRoleTasks" {
  source = "../../modules/hasura/iam-role-task"

  domain_name = var.domain_name
  tags        = var.tags
}
module "IAMRoleCodebuild" {
  source = "../../modules/hasura/iam-role-codebuild"

  domain_name           = var.domain_name
  domain_name_principal = var.domain_name_principal
  tags                  = var.tags
}
module "IAMRoleCodepipeline" {
  source = "../../modules/hasura/iam-role-codepipeline"

  domain_name           = var.domain_name
  domain_name_principal = var.domain_name_principal
  tags                  = var.tags
}
module "CodebuildProject" {
  source = "../../modules/hasura/codebuild-project"

  domain_name    = var.domain_name
  HASURA_VERSION = var.hasura_version
  iam_role_arn   = module.IAMRoleCodebuild.iam_role.arn
  tags           = var.tags
}
module "ECSTaskDefinition" {
  source = "../../modules/hasura/ecs-task-definition"

  domain_name  = var.domain_name
  iam_role_arn = module.IAMRoleTasks.iam_role.arn

  HASURA_GRAPHQL_ACCESS_KEY                       = var.HASURA_ENV.HASURA_GRAPHQL_ACCESS_KEY
  HASURA_GRAPHQL_ENABLE_CONSOLE                   = var.HASURA_ENV.HASURA_GRAPHQL_ENABLE_CONSOLE
  HASURA_GRAPHQL_DATABASE_URL                     = var.HASURA_ENV.HASURA_GRAPHQL_DATABASE_URL
  HASURA_GRAPHQL_SERVER_PORT                      = var.HASURA_ENV.HASURA_GRAPHQL_SERVER_PORT
  HASURA_GRAPHQL_WEBHOOK_URL                      = var.HASURA_ENV.HASURA_GRAPHQL_WEBHOOK_URL
  HASURA_GRAPHQL_WEBHOOK_API_KEY                  = var.HASURA_ENV.HASURA_GRAPHQL_WEBHOOK_API_KEY
  HASURA_GRAPHQL_JWT_SECRET_TYPE                  = var.HASURA_ENV.HASURA_GRAPHQL_JWT_SECRET_TYPE
  HASURA_GRAPHQL_JWT_SECRET_KEY                   = var.HASURA_ENV.HASURA_GRAPHQL_JWT_SECRET_KEY
  HASURA_GRAPHQL_JWT_SECRET_TYPE_CLAIMS_NAMESPACE = var.HASURA_ENV.HASURA_GRAPHQL_JWT_SECRET_TYPE_CLAIMS_NAMESPACE

  tags = var.tags
}
module "ECSService" {
  source = "../../modules/hasura/ecs-service"

  module_depends_on = [
    "${module.LB}",
    "${module.SecurityGroupLoadbalancer}",
    "${module.LBTargetGroup}"
  ]
  domain_name            = var.domain_name
  task_definition_arn    = module.ECSTaskDefinition.ecs_task_definition.arn
  container_cluster_name = var.container_cluster_name
  lb_security_group_id   = module.SecurityGroupLoadbalancer.security_group.id
  lb_target_group_arn    = module.LBTargetGroup.lb_target_group.arn
  lb_subnet_public_ids   = data.aws_subnet_ids.postgresql.ids
  tags                   = var.tags
}
module "Codepipeline" {
  source = "../../modules/hasura/codepipeline"

  s3_ci_cd_artifacts_name = var.ci_cd_artifacts
  fargate_service_name    = module.ECSService.ecs_service.name
  container_cluster_name  = var.container_cluster_name
  domain_name             = var.domain_name
  git_repository_name     = var.repository_name
  iam_role_arn            = module.IAMRoleCodepipeline.iam_role.arn
  tags                    = var.tags
}
  
