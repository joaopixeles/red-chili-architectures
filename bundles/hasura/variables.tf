variable "domain_name_principal" {
  type        = string
  description = "Nombre del dominio al cual va a estar atado el despligue de la pagina web"
}
variable "domain_name" {
  type        = string
  description = "Nombre del dominio al cual va a estar atado el despligue de la pagina web"
}
variable "vpc_id" {
  type        = string
  description = "Buscar el id o crear un data source para buscar el id"
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
variable "hasura_health_check_path" {
  type        = string
  description = "endpoint el cual verificar"
}
variable "container_cluster_name" {
  type        = string
  description = "En ECS el cluster que se creo para el despliegue del servicio de graphql."
}
variable "repository_name" {
  type        = string
  description = "codecommit nombre del repositorio."
}
variable "ci_cd_artifacts" {
  type = string
}
variable "hasura_version" {
  type = string
}
variable "db_port" {
  type = "number"
}
