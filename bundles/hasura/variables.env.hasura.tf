variable "HASURA_ENV" {
  type = object({
    HASURA_GRAPHQL_ACCESS_KEY                       = string
    HASURA_GRAPHQL_ENABLE_CONSOLE                   = bool
    HASURA_GRAPHQL_DATABASE_URL                     = string
    HASURA_GRAPHQL_SERVER_PORT                      = number
    HASURA_GRAPHQL_WEBHOOK_URL                      = string
    HASURA_GRAPHQL_WEBHOOK_API_KEY                  = string
    HASURA_GRAPHQL_JWT_SECRET_TYPE                  = string
    HASURA_GRAPHQL_JWT_SECRET_KEY                   = string
    HASURA_GRAPHQL_JWT_SECRET_TYPE_CLAIMS_NAMESPACE = string
  })
}
