output "acm_certificate" {
  value = module.Certificate.acm_certificate
}
output "route53_record" {
  value = module.Certificate.route53_record
}
output "acm_certificate_validation" {
  value = module.Certificate.acm_certificate_validation
}
output "security_group" {
  value = module.SecurityGroupLoadbalancer.security_group
}
output "security_group_rule" {
  value = module.SecurityGroupRuleDBAccess.security_group_rule
}
output "lb" {
  value = module.LB.lb
}
output "lb_target_group" {
  value = module.LBTargetGroup.lb_target_group
}
output "lb_listener_http" {
  value = module.LBListenerHTTP.lb_listener
}
output "lb_listener_https" {
  value = module.LBListenerHTTPS.lb_listener
}
output "lb_listener_rule" {
  value = module.LBListenerRule.lb_listener_rule
}
output "iam_role" {
  value = module.IAMRole.iam_role
}
output "iam_policy_attachment" {
  value = module.IAMRole.iam_policy_attachment
}
output "route53_loadbalancer" {
  value = module.Route53LoadBalancer.route53_loadbalancer
}
output "iam_role_task" {
  value = module.IAMRoleTasks.iam_role
}
output "iam_policy_attachment_task" {
  value = module.IAMRoleTasks.iam_policy_attachment
}
output "iam_role_codebuild" {
  value = module.IAMRoleCodebuild.iam_role
}
output "iam_role_policy_codebuild" {
  value = module.IAMRoleCodebuild.iam_role_policy
}
output "iam_role_codepipeline" {
  value = module.IAMRoleCodepipeline.iam_role
}
output "iam_role_policy_codepipeline" {
  value = module.IAMRoleCodepipeline.iam_role_policy
}
output "codebuild_project" {
  value = module.CodebuildProject.codebuild_project
}
output "cloudwatch_log_group" {
  value = module.ECSTaskDefinition.cloudwatch_log_group
}
output "ecs_task_definition" {
  value = module.ECSTaskDefinition.ecs_task_definition
}
output "ecs_service" {
  value = module.ECSService.ecs_service
}
output "codepipeline" {
  value = module.Codepipeline.codepipeline
}