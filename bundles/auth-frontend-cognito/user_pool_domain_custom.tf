resource "aws_cognito_user_pool_domain" "custom" {
  domain          = "auth.${var.domain_name}"
  certificate_arn = data.aws_acm_certificate.cognito.arn
  user_pool_id    = aws_cognito_user_pool.default.id
}