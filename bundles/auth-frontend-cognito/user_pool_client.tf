resource "aws_cognito_user_pool_client" "default" {
  name         = "${var.domain_sub}-client"
  user_pool_id = aws_cognito_user_pool.default.id

  read_attributes = [
    "address", "birthdate", "email", "email_verified", "family_name", "gender", "given_name", "locale", "middle_name", "name", "nickname", "phone_number", "phone_number_verified", "picture", "preferred_username", "profile", "updated_at", "website", "zoneinfo"
  ]
  write_attributes = [
    "address", "birthdate", "email", "family_name", "gender", "given_name", "locale", "middle_name", "name", "nickname", "phone_number", "picture", "preferred_username", "profile", "updated_at", "website", "zoneinfo"
  ]

  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = ["https://${var.domain_name}/index.html"]
  logout_urls                          = ["https://${var.domain_name}/index.html"]
  allowed_oauth_flows                  = ["implicit", "code"]
  allowed_oauth_scopes                 = ["aws.cognito.signin.user.admin", "openid"]
  allowed_oauth_flows_user_pool_client = true
}
