data "aws_acm_certificate" "cognito" {
  domain = "auth.${var.domain_name}"
}