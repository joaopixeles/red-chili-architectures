variable "domain_name" { type = string }
variable "domain_name_principal" { type = string }
variable "tags" {
  type = map(any)
}
