provider "aws" {
  region = "us-west-1"
}

provider "aws" {
  alias  = "virginia"
  region = "us-east-1"
}


terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.27.0"
    }
  }
}
