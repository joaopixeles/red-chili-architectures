terraform {
  # backend "s3" {
  #   # Replace this with your bucket name!
  #   key = "global/cross.tfstate"
  #   # Replace this with your DynamoDB table name!
  # }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.27.0"
    }
  }
}
