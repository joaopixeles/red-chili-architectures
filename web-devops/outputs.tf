output "s3_bucket_artifact" {
  value = data.aws_s3_bucket.artifacts
}
output "s3_bucket_root" {
  value = data.aws_s3_bucket.root
}
output "iam_role_codebuild" {
  value = module.IAMRoleCodebuild.iam_role
}
output "iam_role_policy_codebuild" {
  value = module.IAMRolePolicyCodebuild.iam_role_policy
}
output "codebuild_project" {
  value = module.CodebuildProjectWebsite.codebuild_project
}
output "iam_role_codepipeline" {
  value = module.IAMRoleCodepipeline.iam_role
}
output "iam_role_policy_codepipeline" {
  value = module.IAMRolePolicyCodepipeline.iam_role_policy
}
output "codepipeline" {
  value = module.CodepipelineWebsite.codepipeline
}