locals {
  full_domain = var.sub_domian_name == var.domain_name ? "${var.domain_name}.${var.tld}": "${var.sub_domian_name}.${var.domain_name}.${var.tld}"
  bucket_name = var.sub_domian_name == var.domain_name ? "${var.domain_name}-site": "${var.sub_domian_name}-${var.domain_name}-site"
  alternative_domain_names = ["*.${local.full_domain}"]
}

