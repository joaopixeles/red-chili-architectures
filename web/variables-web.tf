variable "ttl" { type = number }
variable "allow_overwrite" { type = bool }
variable "sub_domian_name" { type = string }
variable "domain_name" { type = string }
variable "tld" { type = string }
variable "tags" { type = map(any) }
variable "repository_name" { type = string }