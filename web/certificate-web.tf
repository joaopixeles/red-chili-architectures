module "Certificate" {
  source = "../modules/certificate"

  providers = {
    aws.region : aws.certificate
  }

  sub_domian_name = var.sub_domian_name
  domain_name     = var.domain_name
  tld             = var.tld
  allow_overwrite = var.allow_overwrite
  ttl             = var.ttl
  tags            = var.tags

}
