module "Route53" {
  #**************** MODULO REQUERIDO ******************
  source = "../modules/web-route53"

  sub_domian_name = var.sub_domian_name
  domain_name     = var.domain_name
  tld             = var.tld
  cloudfront_domain_name = aws_cloudfront_distribution.s3_distribution.domain_name
}