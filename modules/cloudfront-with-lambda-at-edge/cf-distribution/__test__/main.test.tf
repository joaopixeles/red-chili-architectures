provider "aws" {
  alias  = "virginia"
  region = "us-east-1"
}

module "CFOriginAccessIdentityTest" {
  source = "../../cloudfront-origin-access-identity"

  domain_name = "test.wvle.joaopixeles.io"
}

module "S3BucketPrivateTest" {
  source = "../../s3-bucket-private"

  domain_name = "test.wvle.joaopixeles.io"
}

module "S3BucketPrivatePilicyTest" {
  source = "../../s3-bucket-private-policy"

  bucket_arn                     = module.S3BucketPrivateTest.output_json.arn
  bucket_id                      = module.S3BucketPrivateTest.output_json.id
  origin_access_identity_iam_arn = module.CFOriginAccessIdentityTest.output_json.iam_arn
}

module "S3BucketPublicTest" {
  source = "../../s3-bucket-public"

  domain_name = "test.wvle.joaopixeles.io"
}

module "S3BucketPublicPilicyTest" {
  source = "../../s3-bucket-public-policy"

  bucket_arn                     = module.S3BucketPublicTest.output_json.arn
  bucket_id                      = module.S3BucketPublicTest.output_json.id
  origin_access_identity_iam_arn = module.CFOriginAccessIdentityTest.output_json.iam_arn
}

module "Certification" {
  source = "../../../cross/acm-certificate"

  providers = {
    aws.certification = aws.virginia
  }

  domain_name              = "test.wvle.joaopixeles.io"
  alternative_domain_names = ["*.test.wvle.joaopixeles.io"]
  ttl                      = "60"
  allow_overwrite          = true
  tags                     = {}
  domain_name_principal    = "joaopixeles.io"
}

module "CloudFrontDistributionTest" {
  source = "../"

  domain_name                   = "test.wvle.joaopixeles.io"
  s3_bucket_private_domain_name = module.S3BucketPrivateTest.output_json.bucket_domain_name
  s3_bucket_public_domain_name  = module.S3BucketPublicTest.output_json.bucket_domain_name
  origin_access_identity        = module.CFOriginAccessIdentityTest.output_json
  certificate_arn               = module.Certification.acm_certificate_validation.certificate_arn
  tags                          = {}
}
