// "https://github.com/cloudposse/terraform-aws-cloudfront-cdn/blob/master/main.tf",
// "https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html",
resource "aws_cloudfront_distribution" "default" {
  aliases             = [var.domain_name]
  enabled             = true
  default_root_object = "index.html"
  price_class         = "PriceClass_All"
  origin {
    origin_id   = "${var.domain_name}-private-access"
    domain_name = var.s3_bucket_private_domain_name

    s3_origin_config {
      origin_access_identity = var.origin_access_identity.cloudfront_access_identity_path
    }
  }
  origin {
    origin_id   = "${var.domain_name}-public-access"
    domain_name = var.s3_bucket_public_domain_name

    s3_origin_config {
      origin_access_identity = var.origin_access_identity.cloudfront_access_identity_path
    }
  }
  default_cache_behavior {
    target_origin_id       = "${var.domain_name}-public-access"
    viewer_protocol_policy = "redirect-to-https"
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]

    forwarded_values {
      query_string = false
      headers      = ["Origin"]
      cookies {
        forward = "none"
      }
    }
  }
  ordered_cache_behavior {
    path_pattern     = "private/*"
    target_origin_id = "${var.domain_name}-private-access"
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]

    lambda_function_association {
      event_type   = "viewer-request"
      include_body = false
      lambda_arn   = var.lambda_function_arn
    }

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "redirect-to-https"
  }
  ordered_cache_behavior {
    path_pattern     = "public/*"
    target_origin_id = "${var.domain_name}-public-access"
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "redirect-to-https"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }
  viewer_certificate {
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
    acm_certificate_arn      = var.certificate_arn
  }
  tags = var.tags
}



