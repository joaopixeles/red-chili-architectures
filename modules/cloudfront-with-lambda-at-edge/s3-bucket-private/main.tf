resource "aws_s3_bucket" "private_bucket" {
  bucket = var.bucket_name
  acl    = "private"

  cors_rule {
    allowed_origins = ["http*"]
    allowed_methods = ["HEAD", "GET", "PUT", "POST", "DELETE"]
    allowed_headers = ["*"]
    expose_headers  = ["ETag", "x-amz-meta-custom-header"]

  }
}
