resource "aws_s3_bucket_policy" "private_bucket_policy" {
  bucket = var.bucket_id
  policy= data.aws_iam_policy_document.s3_policy.json
}