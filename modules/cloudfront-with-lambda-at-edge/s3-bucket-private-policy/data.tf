data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${var.bucket_arn}/*"]
    principals {
      type        = "AWS"
      identifiers = [var.origin_access_identity_iam_arn]
    }

  }
  version = "2012-10-17"
}



