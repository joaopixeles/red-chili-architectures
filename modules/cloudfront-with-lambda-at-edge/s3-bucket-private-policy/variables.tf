variable "bucket_arn" {
  type = string
}
variable "origin_access_identity_iam_arn" {
  type = string
}
variable "bucket_id" {
  type = string
}
