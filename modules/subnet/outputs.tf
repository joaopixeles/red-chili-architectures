output "subnet_ids" {
  value = [for v in aws_subnet.subnet : v.id]
}