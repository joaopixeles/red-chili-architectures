SubnetsGraphqlTest__Zone_1 = {
  "arn" = "arn:aws:ec2:us-east-1:314418232935:subnet/subnet-02f1ae3fa18899772"
  "assign_ipv6_address_on_creation" = false
  "availability_zone" = "us-east-1a"
  "availability_zone_id" = "use1-az1"
  "cidr_block" = "172.31.112.0/20"
  "id" = "subnet-02f1ae3fa18899772"
  "ipv6_cidr_block" = ""
  "ipv6_cidr_block_association_id" = ""
  "map_public_ip_on_launch" = false
  "owner_id" = "314418232935"
  "tags" = {
    "Name" = "dev.tomate.chat"
  }
  "vpc_id" = "vpc-b82be2dd"
}
SubnetsGraphqlTest__Zone_2 = {
  "arn" = "arn:aws:ec2:us-east-1:314418232935:subnet/subnet-035ceaecca6bae997"
  "assign_ipv6_address_on_creation" = false
  "availability_zone" = "us-east-1b"
  "availability_zone_id" = "use1-az2"
  "cidr_block" = "172.31.128.0/20"
  "id" = "subnet-035ceaecca6bae997"
  "ipv6_cidr_block" = ""
  "ipv6_cidr_block_association_id" = ""
  "map_public_ip_on_launch" = false
  "owner_id" = "314418232935"
  "tags" = {
    "Name" = "dev.tomate.chat"
  }
  "vpc_id" = "vpc-b82be2dd"
}
SubnetsGraphqlTest__Zone_3 = {
  "arn" = "arn:aws:ec2:us-east-1:314418232935:subnet/subnet-03768907e829cf599"
  "assign_ipv6_address_on_creation" = false
  "availability_zone" = "us-east-1c"
  "availability_zone_id" = "use1-az4"
  "cidr_block" = "172.31.144.0/20"
  "id" = "subnet-03768907e829cf599"
  "ipv6_cidr_block" = ""
  "ipv6_cidr_block_association_id" = ""
  "map_public_ip_on_launch" = false
  "owner_id" = "314418232935"
  "tags" = {
    "Name" = "dev.tomate.chat"
  }
  "vpc_id" = "vpc-b82be2dd"
}
SubnetsGraphqlTest__subnet_ids = [
  "subnet-02f1ae3fa18899772",
  "subnet-035ceaecca6bae997",
  "subnet-03768907e829cf599",
]