resource "aws_subnet" "subnet" {
  for_each                = var.subnets
  vpc_id                  = var.vpc_id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.availability_zone
  map_public_ip_on_launch = var.auto_assing_public_ip
  tags                    = merge(var.tags, { "Name" = "${var.prefix}-${replace(var.domain_name, ".", "-")}" })
}