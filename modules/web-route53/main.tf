resource "aws_route53_record" "default" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = local.full_domain
  type    = "A"
  alias  {
    name                   = "${var.cloudfront_domain_name}"
    zone_id                = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
}