# asb acronym aws_s3_bucket",
variable "domain_name" {
  description = "Domain name for your devops (example.com)"
  type        = string
  default     = ""
}
variable "WWWBucket" {
  type    = bool
  default = false
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
