resource "aws_iam_role_policy" "default" {
  name   = replace(var.iam_role_name, ".", "-")
  role   = var.iam_role_id
  policy = templatefile("${path.module}/airp_policy.json", local.template_var)
}
