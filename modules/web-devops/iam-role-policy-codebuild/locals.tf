locals {
  template_var = {
    s3_artifact_arn = var.s3_artifact_arn
    s3_root_arn     = var.s3_root_arn
    aws_region      = data.aws_region.current.name
    aws_account_id  = data.aws_caller_identity.current.account_id
  }
}
