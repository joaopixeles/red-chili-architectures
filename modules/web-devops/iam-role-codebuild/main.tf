resource "aws_iam_role" "default" {
  name               = "${var.name}-codebuild"
  assume_role_policy = file("${path.module}/air_assume_role_policy.json")
  path               = "/"
  tags               = var.tags
}
