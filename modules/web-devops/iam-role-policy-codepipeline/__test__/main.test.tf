module "IAMRolePolicyCodepipeline" {
  // "---------------- MODULO A PROBAR -------------------"
  source = "../"

  iam_role_name   = module.IAMRoleCodepipeline.iam_role.name
  iam_role_id     = module.IAMRoleCodepipeline.iam_role.id
  s3_artifact_arn = module.BucketArtifact.s3_bucket.arn
}

// "**************** MODULO & RECURSOS REQUERIDOS ******************"
module "BucketArtifact" {
  source = "../../../bucket-artifacts"

  providers = {
    aws.region = aws
  }

  domain_name           = "test-devops.joaopixeles.io"
  domain_name_principal = "joaopixeles.io"
  suffix                = "artifacts"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}
module "IAMRoleCodepipeline" {
  source = "../../iam-role-codepipeline"

  domain_name = "test-devops.joaopixeles.io"
  tags        = {}
}
