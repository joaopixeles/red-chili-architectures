
variable "iam_role_id" {
  type        = string
  description = "role_id"
}
variable "iam_role_name" {
  type        = string
  description = "El nombre del role es igual que el del dominio"
}
variable "s3_artifact_arn" {
  type = string
}