resource "aws_codepipeline" "default" {
  role_arn = var.iam_role_arn
  stage {
    name = "Source"

    action {
      name     = "SourceAction"
      category = "Source"
      owner    = "AWS"
      version  = "1"
      provider = "CodeCommit"

      output_artifacts = ["StaticSiteSource"]

      configuration = {
        BranchName     = var.repository_branch
        RepositoryName = var.repository_name
      }
    }

  }
  stage {
    name = "Build"

    action {
      name            = "BuildRoot"
      input_artifacts = ["StaticSiteSource"]
      category        = "Build"
      owner           = "AWS"
      version         = "1"
      provider        = "CodeBuild"

      configuration = {
        ProjectName = var.codebuild_project_name
      }
      run_order = 1
    }
  }


  name = var.name

  artifact_store {
    location = var.s3_artifact_name
    type     = "S3"
  }
  tags = var.tags
}
