// "ac acronym aws_codepipeline"
variable "iam_role_arn" {
  type        = string
  description = "arn_codepipeline_role"
}
variable "repository_name" {
  type        = string
  description = "codecommit_repo_name"
}
variable "codebuild_project_name" {
  type        = string
  description = "codebuild_project_name"
}
variable "s3_artifact_name" {
  type        = string
  description = "codebuild_project_name"
}
variable "repository_branch" {
  type = string
}
variable "name" {
  type = string
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
