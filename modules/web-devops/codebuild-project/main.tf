resource "aws_codebuild_project" "default" {
  artifacts {
    type = "CODEPIPELINE"
  }
  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/ubuntu-base:14.04"
    type         = "LINUX_CONTAINER"
    environment_variable {
      name  = "BUCKET_NAME"
      value = var.BUCKET_NAME
    }

  }
  name         = var.name
  description  = "Codebuild for ${var.name}"
  service_role = var.iam_role_arn
  source {
    type            = "CODEPIPELINE"
    buildspec       = var.buildspec_path
    git_clone_depth = 1
  }

  # secondary_sources {
  #   type              = "CODEPIPELINE"
  #   buildspec         = var.buildspec_path
  #   source_identifier = "ROOT"
  # }

  # logs_config {
  #   s3_logs {
  #     status   = "ENABLED"
  #     location = "serendipia-organization-artifacts/build-log"
  #   }
  # }
  build_timeout = "10"
  tags          = var.tags
}


