// "**************** MODULO & RECURSOS REQUERIDOS ******************"

module "IAMRoleCodebuild" {
  source = "../../iam-role-codebuild"

  domain_name = "test-devops.joaopixeles.io"
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}


resource "aws_s3_bucket" "ex" {
  bucket = "test-devops.joaopixeles.io"
  acl    = "public-read"

  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}

module "CodebuildProjectWebsiteTest" {
  // "---------------- MODULO A PROBAR -------------------"
  source = "../"

  BUCKET_NAME  = aws_s3_bucket.ex.id
  name  = "joaopixeles-io-test-devops"
  iam_role_arn = module.IAMRoleCodebuild.iam_role.arn
  tags = {
    Name        = "Terraform Test"
    Environment = "Dev"
  }
}

