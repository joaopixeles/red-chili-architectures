variable "BUCKET_NAME" {
  type = string
}
variable "buildspec_path" {
  type = string
}
variable "name" {
  type = string
}
variable "iam_role_arn" {
  type        = string
  description = "arn_codebuild_role"
}
variable "tags" {
  type        = map(string)
  description = "Tags para el manejo de recursos y tener mejor control de costos"
}
