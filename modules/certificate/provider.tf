# terraform {
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = ">= 3.27.0"
#     }
#   }
# }

# provider "aws" {
#   alias = "certification"
# }


terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      configuration_aliases = [ aws.region ]
    }
  }
}