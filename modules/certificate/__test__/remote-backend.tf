terraform {
  backend "s3" {
    bucket = "serendipia-organization-terraform"
    key = "demosdemos/modules/certificate.tfstate"
    region = "us-east-1"

    profile                  = "serendipia"
    dynamodb_table = "serendipia-organization-terraform-locks"
    encrypt = true
  }
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs
provider "aws" {
  shared_config_files      = ["$HOME/.aws/config"]
  shared_credentials_files = ["$HOME/.aws/credentials"]
  profile                  = "serendipia"
}

provider "aws" {
  alias  = "certification"
  region = "us-east-1"
}
