resource "aws_s3_bucket" "default" {
  tags     = var.tags
  bucket   = local.bucket_name
}

resource "aws_s3_bucket_acl" "default" {
  bucket = aws_s3_bucket.default.id
  acl    = "private"
}
