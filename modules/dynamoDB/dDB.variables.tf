variable "domain_name" { type = string }
variable "alternative_domain_names" { type = list(string) }
variable "ttl" { type = number }
variable "allow_overwrite" { type = bool }
variable "tags" { type = map(string) }
variable "domain_name_principal" { type = string }
